/*
 * PreconditionViolationException.h
 *
 *  Created on: Dec 5, 2016
 *      Author: jdaehn
 */

#ifndef PRE_CONDITION_VIOLATION_EXCEPTION_H_
#define PRE_CONDITION_VIOLATION_EXCEPTION_H_

#include <string>
#include "CSC232CustomException.h"

class PreconditionViolationException: public CSC232CustomException {
public:
	PreconditionViolationException(const std::string& message = "");
};

#endif /* PRE_CONDITION_VIOLATION_EXCEPTION_H_ */
