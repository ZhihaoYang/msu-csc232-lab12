/*
 * PreconditionViolationException.cpp
 *
 *  Created on: Dec 5, 2016
 *      Author: jdaehn
 */

#include "PreconditionViolationException.h"

PreconditionViolationException::PreconditionViolationException(
		const std::string& message) :
		CSC232CustomException("Precondition violation exception: " + message) {
	// no-op; delegate to CSC232CustomException
}
