#Lab 12 - Working with Trees

##Background

In this lab we explore the concept of tree traversals. Previously, the structures we explored were all linear in nature, i.e., they had a well-defined predecessor and well-defined successor. As such, traversing these structures is trivial. Now, with tree structures storing some collection of data, such structure traversal is no longer trivial. Three different traversal algorithms are explored in this lab:

* Preorder
* Inorder
* Postorder

### Tree traversals

Recall the formal definition of a binary tree:

A binary tree is a set _T_ of nodes that is either empty or partitioned into disjoint subsets:

* A single node, _r_, the root
* Two (possibly empty) sets that are binary trees, called **left** and **right subtrees** of _r_

Notice this is a recursive definition. With this recursive definition in mind, we can construct a recursive traversal algorithm as follows:

* If _T_ is empty, the traversal algorithm takes no action -- an empty tree is the base case.
* If _T_ is not empty, the traversal algorithm must peform three tasks:
  ** It must display the data in the root _r_,
  ** It must traverse the two subtrees _Tl_ and _Tr_ each of which is a binary tree smaller than _T_.

#### Preorder Traversal

The recursive preorder traversal algorithm is as follows:

```

    // Traverses the given binary tree in preorder
    // Assumes that "visit a node" means to process the node's data item
    preorderTraverse(binTree: BinaryTree) : void
    {
        if (binTree is not empty)
        {
            Visit the root of binTree
            preorderTraverse(Left subtree of binTree's root)
            preorderTraverse(Right subtree of binTree's root)
        }
    }

```

#### Inorder Traversal

The recursive inorder traversal algorithm is as follows:

```

    // Traverses the given binary tree in inorder
    // Assumes that "visit a node" means to process the node's data item
    inorderTraverse(binTree: BinaryTree) : void
    {
        if (binTree is not empty)
        {
            inorderTraverse(Left subtree of binTree's root)
            Visit the root of binTree
            inorderTraverse(Right subtree of binTree's root)
        }
    }

```

#### Postorder Traversal

The recursive postorder traversal algorithm is as follows:

```

    // Traverses the given binary tree in postorder
    // Assumes that "visit a node" means to process the node's data item
    postorderTraverse(binTree: BinaryTree) : void
    {
        if (binTree is not empty)
        {
            postorderTraverse(Left subtree of binTree's root)
            postorderTraverse(Right subtree of binTree's root)
            Visit the root of binTree
        }
    }

```

If you look closely at `BinaryNodeTree.h`, you'll see that the public-facing operations for tree traversal are declared as follows:

```

    void preorderTraverse(void visit(ItemType&)) const;
    void inorderTraverse(void visit(ItemType&)) const;
    void postorderTraverse(void visit(ItemType&)) const;
    
```

These are interesting method signatures. The argument to each of these member functions is _function signature_. This means we can pass a _function name_ to these methods so long as that _function name_ is of type `void` and has a single argument of type `ItemType&`. Look at the function defined on line 12 of `Lab12.cpp` -- this function fits the bill:

```

    void display(std::string& anItem) {
        ...
    }
    
```

This function, named `display`, is of type `void` and it takes a single argument of type `std::string&` -- which is the same type we are instantiating `tree1` with, and hence, is the `ItemType&` we need. Notice how this function name is passed as an argument to `preorderTraverse` on line 36 of `Lab12.cpp`.

Now open `BinaryNodeTree.cpp` and look at the definition of `preorderTraverse`:

```

    template<class ItemType>
    void BinaryNodeTree<ItemType>::preorderTraverse(void visit(ItemType&)) const {
	    preorder(visit, rootPtr);
    }  // end preorderTraverse
    
```

Notice it calls a helper function `preorder` passing it a function to use when "visiting" a node, and a pointer to the root node of the tree to traverse. In the three tasks outlined below, it is your job to provide the implementation details of these helper functions. DO NOT modify the public-facing operations. It is these helper functions that are implemented in the recursive manner described above. When it comes time to "visit" a node, simply invoke the function `visit` passing in the item stored in the root pointer.

##Tasks

## Part 1 - Preorder Traversal

This part is worth 1 point.

1. Locate the `TODO` in `BinaryNodeTree.cpp` that corresponds to this task (Part 1). Implement the `preorder` member function using recursion to traverse the given tree using the passed in `visit` function in the appropriate location of your implementation. **NOTE**: You are only to modify the body of this method, and no others in your solution to this task.
1. When you have successfully completed this task, commit your changes using the following commit message: "CSC232-LAB12 - Completed Task 1."

## Part 2 - Inorder Traversal

This part is worth 1 point. 

1. Locate the `TODO` in `BinaryNodeTree.cpp` that corresponds to this task (Part 2). Implement the `inorder` member function using recursion to traverse the given tree using the passed in `visit` function in the appropriate location of your implementation. **NOTE**: You are only to modify the body of this method, and no others in your solution to this task.
1. When you have successfully completed this task, commit your changes using the following commit message: "CSC232-LAB12 - Completed Task 2."

## Part 3 - Postorder Traversal

This part is worth 1 point.

1. Locate the `TODO` in `BinaryNodeTree.cpp` that corresponds to this task (Part 3). Implement the `postorder` member function using recursion to traverse the given tree using the passed in `visit` function in the appropriate location of your implementation. **NOTE**: You are only to modify the body of this method, and no others in your solution to this task.
1. When you have successfully completed this task, commit your changes using the following commit message: "CSC232-LAB12 - Completed Task 3."

##Due Date

**Every** individual must complete this lab by submitting the Lab 12 assignment on Blackboard by 23:59 Friday 09 December 2016. To get full credit, you must include a Text submission that

* has a valid, working, hyperlink to your pull request for the work done in this lab.
* identifies every member of your team.
* (optionally) identifies any issues you had in executing the requirements of this lab.
